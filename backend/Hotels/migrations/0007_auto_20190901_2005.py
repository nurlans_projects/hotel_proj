# Generated by Django 2.2.4 on 2019-09-01 20:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Hotels', '0006_auto_20190827_2008'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Cities',
            new_name='City',
        ),
        migrations.RenameModel(
            old_name='Hotels',
            new_name='Hotel',
        ),
    ]
