from django.test import TestCase
from ..models import City

class testModels (TestCase):

    def setUp(self):
        self.city = City.objects.create(
            cityName="Barselona",
            cityId="BAR"
        )

    def testCityID(self):
        self.cityId = self.city.get_city_id()
        self.assertEqual(self.cityId, "BAR")

    def testCityName(self):
        self.cityName = self.city.get_city_name()
        self.assertEqual(self.cityName, "Barselona")



# class testUrls (TestCase):
#     def test_first_app(self):
#         assert 1 == 1
#
#     def test_add(self):
#         assert test_first_add(1,2) == 3
#
#     def testCityName(self):
#
#         self.project = Cities.objects.create(
#                 cityName="Barselona",
#                 cityId="BAR"
#             )
#
#         self.bar = self.project.get_cityId()
#
#
#
#         self.assertEqual(self.bar, "Barselona")