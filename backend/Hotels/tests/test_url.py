from django.urls import resolve, reverse
from django.test import TestCase
from ..views import index
from ..api.views import CitiesListView, CitiesDetailListView, HotelListView, HotelDetailListView, FilterHotelList

class HomePageTest(TestCase):

    def test_home_page_url(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_cities_page(self):
        url = reverse('cities')
        self.assertEqual(resolve(url).func.view_class, CitiesListView)

    def test_city_detail_page(self):
        url = reverse('city_detail', args=['some_arg'])
        self.assertEqual(resolve(url).func.view_class, CitiesDetailListView)

    def test_hotels_page(self):
        url = reverse('hotels')
        self.assertEqual(resolve(url).func.view_class, HotelListView)

    def test_hotel_detail_page(self):
        url = reverse('hotel_detail', args=['some_arg'])
        self.assertEqual(resolve(url).func.view_class, HotelDetailListView)

    def test_filter_hotel_page(self):
        url = reverse('filtering_hotels')
        self.assertEqual(resolve(url).func.view_class, FilterHotelList)