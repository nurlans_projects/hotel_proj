from django.urls import path, include
from . import views
# from .api import post_data

urlpatterns = [
    path('', views.index),
    # path('upload', post_data.get_data),
    path('api/', include('Hotels.api.urls'))
]