from django.db import models

class City(models.Model):
    cityId = models.CharField(max_length=20)
    cityName = models.CharField(max_length=40)

    def get_city_name(self):
        return self.cityName

    def get_city_id(self):
        return self.cityId

    def __str__(self):
        return self.cityName



class Hotel(models.Model):
    cityId = models.CharField(max_length=20)
    hotelNo = models.CharField(max_length=40)
    hotelName = models.TextField(max_length=60)

    def __str__(self):
        return self.hotelName
