from django.contrib import admin
from .models import  City, Hotel

#registered Cities and Hotel table in database
admin.site.register(City)
admin.site.register(Hotel)