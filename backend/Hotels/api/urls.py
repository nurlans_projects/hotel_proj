from django.urls import path

from .views import CitiesListView, CitiesDetailListView, HotelListView, HotelDetailListView, FilterHotelList, GetCitiesCount,GetHotelCount

urlpatterns = [
    path('cities', CitiesListView.as_view(), name='cities'),
    path('city/<pk>', CitiesDetailListView.as_view(), name='city_detail'),
    path('hotels/', HotelListView.as_view(), name='hotels' ),
    path('hotel/', FilterHotelList.as_view(), name='filtering_hotels'),
    path('hotels/<pk>', HotelDetailListView.as_view(), name='hotel_detail'),
    path('city/count/', GetCitiesCount.as_view(), name='city_count'),
    path('hotels/count/', GetHotelCount.as_view(), name='hotel_count')

]