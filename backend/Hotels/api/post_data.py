import requests, csv
from requests.auth import HTTPBasicAuth
from contextlib import closing
import codecs
import sqlite3

#get city data from url and save in  city_data [] list
def get_city_datas():
	cityUrl= 'http://rachel.maykinmedia.nl/djangocase/city.csv'
	username = 'python-demo'
	password = 'claw30_bumps'
	city_data = []

	with closing(requests.get(cityUrl, auth=HTTPBasicAuth(username, password), stream=True)) as r:
		cityReader = csv.reader(codecs.iterdecode(r.iter_lines(), 'utf-8'), delimiter=';')
		for row in cityReader:
			city_data.append(row)
	return city_data

#get Hotels data from url and save in hotel_data[] list
def get_hotel_datas():
	hotelUrl = 'http://rachel.maykinmedia.nl/djangocase/Hotels.csv'
	username = 'python-demo'
	password = 'claw30_bumps'
	hotel_data = []

	with closing(requests.get(hotelUrl, auth=HTTPBasicAuth(username, password), stream=True)) as r:
		hotelReader = csv.reader(codecs.iterdecode(r.iter_lines(), 'utf-8'), delimiter=';')
		for row in hotelReader:
			hotel_data.append(row)
	return hotel_data


#post all data if database is empty ...
def post_data ():
	city  = get_city_datas()
	hotel = get_hotel_datas()

	#connect with sqlite file to send data
	# conn = sqlite3.connect('/Users/shahla/Documents/mysite/backend/db.sqlite3')
	conn = sqlite3.connect('/app/db.sqlite3')
	cur = conn.cursor()
	city_count = cur.execute('SELECT COUNT (*) from hotels_cities').fetchall()[0][0]
	hotel_count = cur.execute('SELECT COUNT (*) from hotels_hotels').fetchall()[0][0]

	if city_count == 0:
		for i in range(len(city)):
			cur.execute("INSERT INTO hotels_cities ('CityId', 'CityName') VALUES (?,?)", (city[i][0], city[i][1]))
	else:
		pass
		# TODO: This part needs more sophisticated logic. Intentionally skipped do to time limit

	if hotel_count == 0:
		for i in range(len(hotel)):
			cur.execute("INSERT INTO hotels_hotels ('CityId', 'HotelNo','HotelName') VALUES (?,?,?)", (hotel[i][0], hotel[i][1], hotel[i][2]))
	else:
		pass
		# TODO: This part needs more sophisticated logic. Intentionally skipped do to time limit

	conn.commit()



print('UPLOADING...')
post_data()