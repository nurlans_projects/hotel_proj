from rest_framework.generics import ListAPIView, RetrieveAPIView
from ..models import City, Hotel
from django.db.models import Q
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from .serializers import CitiesSerializer, HotelSerializer, FilterHotelSerializer

# build API views


class GetCitiesCount(ListAPIView):

    def get(self, request, format=None):
        hotel_count = City.objects.count()
        content = {"city_count": hotel_count}
        return Response(content)

class GetHotelCount(ListAPIView):

    def get(self, request='count'):
        hotel_count = Hotel.objects.count()
        content = {"hotel_count": hotel_count}
        return Response(content)

class CitiesListView (ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitiesSerializer

class CitiesDetailListView (RetrieveAPIView):
    queryset = City.objects.all()
    serializer_class = CitiesSerializer

class HotelListView (ListAPIView):
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializer

class HotelDetailListView (RetrieveAPIView):
    queryset = Hotel.objects.all()
    serializer_class = HotelSerializer


class FilterHotelList(ListAPIView):

    serializer_class = FilterHotelSerializer
    pagination_class = LimitOffsetPagination

    def get_queryset(self, *args, **kwargs):

        queryset = Hotel.objects.all()
        query = self.request.GET.get("q")

        if query:
            queryset = queryset.filter(
                Q(cityId__contains=query)
            ).distinct()
        return  queryset


