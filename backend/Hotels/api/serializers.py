from rest_framework import serializers
from ..models import City, Hotel

# Serializing objects in json format for implementing the REST API

class CitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = ('id','cityId','cityName')

class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ('id','cityId','hotelNo','hotelName')

class FilterHotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ('cityId','hotelNo','hotelName')


