import { Select } from "antd";
import "antd/dist/antd.css";
import React from "react";

const { Option } = Select;

class SearchData extends React.Component {
  state = {
    cities: [],
    searchText: "",
    datas: []
  };

  onChange = (key, value) => {
    if (this.props.onCityChange) {
      if (value) {
        this.props.onCityChange(value.key);
      } else {
        this.props.onCityChange(null);
      }
    }
  };

  loadCities = async () => {
    let url = "http://localhost:8000/api/cities";
    const response = await fetch(url);
    const data = await response.json();
    this.setState({ datas: data.results });
  };

  componentDidMount() {
    this.loadCities();
  }

  render() {
    return (
      <div>
        <Select
          allowClear={true}
          size={"large"}
          showSearch={true}
          style={{ width: 230 }}
          placeholder="Choose city or search"
          optionFilterProp="children"
          onChange={this.onChange}
          filterOption={(input, option) =>
          option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=0
          }
        >
          {this.state.datas.map((value, index) => {
            return (
              <Option key={value.cityId} value={value.cityName}>
                {value.cityName}
              </Option>
            );
          })}
        </Select>
      </div>
    );
  }
}
export default SearchData;