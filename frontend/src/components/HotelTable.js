import React from "react";
import { Table } from "antd";
import "antd/dist/antd.css";
import "../static/table.css";
import SearchData from "./SearchHotel.js";
import reqwest from "reqwest";

class Hotels extends React.Component {
  state = {
    searchText: "",
    data: [],
    pagination: {
      pageSize: 10,
      current: 1
    }
  };

  handleTableChange = (pagination, filters, sorter) => {
    console.log("handleTableChange", pagination, filters, sorter);

    const pager = { ...this.state.pagination };

    if (this.state.cityId) {
      filters.q = this.state.cityId;
    }

    pager.current = pagination.current;
    this.setState({
      pagination: pager
    });

    this.loadHotels({
      limit: pagination.pageSize,
      offset: (pagination.current - 1) * pagination.pageSize,
      ...filters
    });
  };

  componentDidMount() {
    this.handleTableChange(this.state.pagination, {}, {});
  }

  loadHotels = async (params = {}) => {
    console.log("params:", params);
    this.setState({ loading: true });

    let url = "http://localhost:8000/api/hotel/";

    const response = await reqwest({
      url: url,
      method: "get",
      data: {
        format: "json",
        ...params
      }
    });

    const pagination = { ...this.state.pagination };
    pagination.total = response.count;
    this.setState({
      loading: false,
      data: response.results,
      pagination
    });
  };

  onCityChange = cityId => {
    this.setState(
      {
        cityId: cityId,
        pagination: {
          ...this.state.pagination,
          current: 1
        }
      },
      () => this.handleTableChange(this.state.pagination, {}, {})
    );
  };

  render() {
    const columns = [
      {
        title: "CITY ID",
        dataIndex: "cityId",
        key: "cityId",
        width: "30%"
      },
      {
        title: "NUMBER",
        dataIndex: "hotelNo",
        key: "hotelNo",
        width: "30%"
      },
      {
        title: "HOTEL NAME",
        dataIndex: "hotelName",
        key: "hotelName",
        width: "30%"
      }
    ];

    return (
      <div>
        <div>
          <h1>HOTELS LIST </h1>
          <SearchData
            style={{ float: "left" }}
            onCityChange={this.onCityChange}
          />

          <br />

          <Table
            dataSource={this.state.data}
            pagination={this.state.pagination}
            loading={this.state.loading}
            onChange={this.handleTableChange}
            bordered
            columns={columns}
            rowKey="hotelNo"
          />
        </div>
      </div>
    );
  }
}

export default Hotels;