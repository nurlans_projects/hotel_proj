import React from 'react';
import Hotels from './components/HotelTable.js'
import './App.css';

function App() {
  return (
    <div className="App">
      <Hotels />
    </div>
  );
}
export default App;