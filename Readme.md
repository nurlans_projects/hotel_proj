#Hotels Project


## Run locally

In this project, docker-compose is used to spin up the following components:

- Frontend (react.js)
- Backend (django)
- Cron (sync database)

```shell
docker-compose up -d
```

Note: you should have docker-compose installed on your local machine


Once docker-compose has started:

- [Frontend](http://localhost:3000)
- [Backend](http://localhost:8000)

Backend has the following APIs:

- [Cities](http://localhost:8000/api/cities)
- [Hotels](http://localhost:8000/api/hotels/)
- [Admin](http://localhost:8000/admin/)

Admin panel:

 - *Username:* admin
 - *Password:* hotel777